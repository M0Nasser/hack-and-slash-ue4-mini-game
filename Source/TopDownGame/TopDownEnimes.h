// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TopDownEnimes.generated.h"

UCLASS()
class TOPDOWNGAME_API ATopDownEnimes : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ATopDownEnimes();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = EnimyVariables, meta = (AllowPrivateAccess = "true"))
		float fDistance;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Attack, meta = (AllowPrivateAccess = "true"))
		float fMaxHealth;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Attack, meta = (AllowPrivateAccess = "true"))
		float fCurrentHealth;
	UFUNCTION(BlueprintCallable)
		void TakeDamage(float value);

	void DestroyAfterTime();

	FTimerHandle MemberTimerHandle;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
