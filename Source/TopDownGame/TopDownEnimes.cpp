// Fill out your copyright notice in the Description page of Project Settings.


#include "TopDownEnimes.h"
#include "TimerManager.h"

// Sets default values
ATopDownEnimes::ATopDownEnimes()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	fDistance = 20;
	fMaxHealth = 100;
	fCurrentHealth = fMaxHealth;

}


// Called when the game starts or when spawned
void ATopDownEnimes::BeginPlay()
{
	Super::BeginPlay();

	
}

// Called every frame
void ATopDownEnimes::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ATopDownEnimes::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}


void ATopDownEnimes::TakeDamage(float value)
{
	if (fCurrentHealth > 0)
		fCurrentHealth -= value;
	else
	{
		GetWorldTimerManager().SetTimer(MemberTimerHandle, this, &ATopDownEnimes::DestroyAfterTime, 1.0f, true, 0.5f);

	}
}

void ATopDownEnimes::DestroyAfterTime()
{
	
		this->Destroy();
		
}


